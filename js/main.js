function blocksController($scope) {
    $scope.blocks = [
    ];
    $scope.relations = [];
    $scope.addBlock = function() {
		if ($scope.block.class) {
			$scope.blocks.push({
				class: $scope.block.class, 
				top: 0, 
				left: 0, 
				elements: []
			});
			$scope.block = {};
		}
	}
	
	$scope.removeBlock = function(b) {
		$scope.blocks.splice($scope.blocks.indexOf(b), 1);
	}
	$scope.export = function() {
		console.log(angular.toJson($scope.blocks));
	}
	$scope.dragStart = function(b, $event) {
		$('body').addClass('unselectable');
		$scope.nowDragged = b;
		$scope.dragOffset = {
			left: $event.pageX - b.left, 
			top: $event.pageY - b.top
		};
		
	
	}
	
	$scope.dragStop = function() {
		$('body').removeClass('unselectable');
		$scope.nowDragged = undefined;
		$scope.currentRelation = undefined;
		var canvas = document.getElementById('canvas');
		canvas.width = canvas.width;
		drawRelations();
	}
	$scope.onDrag = function($event) {
		if ($scope.nowDragged != undefined) {
			$scope.nowDragged.left = $event.pageX - $scope.dragOffset.left
			$scope.nowDragged.top = $event.pageY - $scope.dragOffset.top
		}
		if ($scope.currentRelation != undefined) {
			drawRelation($scope.currentRelation, $event);
			drawRelations();
		}
		
	}
	
	$scope.beginRelation = function(e, obj, event) {
		$('body').addClass('unselectable');
		$scope.currentRelation = {
			begin: [e, event],
			end:undefined
		};
	}
	$scope.endRelation = function(e, obj, event) {
		$('body').removeClass('unselectable');
		if (typeof $scope.currentRelation != 'undefined') {
			$scope.currentRelation.end = [e, event];
			drawRelation($scope.currentRelation);
			$scope.relations.push($scope.currentRelation);
		}
		drawRelations();
	}
	var drawRelation = function(relation, $event) {
		var canvas = document.getElementById('canvas');
		canvas.width = canvas.width;
		context = canvas.getContext('2d');
		//~ context.clearRect(0,0, canvas.width, canvas.height);
		context.moveTo(relation.begin[1].clientX-$('#canvas').offset().left, relation.begin[1].clientY-$('#canvas').offset().top);
		if (typeof relation.end == 'undefined') {
			context.lineTo($event.clientX-$('#canvas').offset().left, $event.clientY-$('#canvas').offset().top);
		} else {
			context.lineTo(relation.end[1].clientX-$('#canvas').offset().left, relation.end[1].clientY-$('#canvas').offset().top);
		}
		context.stroke();
	}
	var drawRelations = function(){
		$.each($scope.relations, function(index, relation) {
			var canvas = document.getElementById('canvas');
			context = canvas.getContext('2d');
			context.moveTo(relation.begin[1].clientX-$('#canvas').offset().left, relation.begin[1].clientY-$('#canvas').offset().top);
			context.lineTo(relation.end[1].clientX-$('#canvas').offset().left, relation.end[1].clientY-$('#canvas').offset().top);
			context.stroke();
		});
	}

};

function blockController($scope) {
	$scope.addContent = function(block) {
		if (block.parameter) {
			block.elements.push(
				{
					label: block.parameter
				}
			);
			block.parameter = '';
		}
	}
	$scope.removeContent = function(element) {
		$scope.b.elements.splice($scope.b.elements.indexOf(element), 1);
	}
	
}


